## Help tab
[Back to top of page](#options-page)

![](/res/abp/desktop-settings/help-tab.jpg)

1. [Help tab headline](#help-tab-headline)
1. [Help tab description](#help-tab-description)
1. [Support section](#support-section)
1. [Get in touch section](#get-in-touch-section)

#### Help tab: Headline
`Help`

Navigation label will match headline.

#### Help tab: Description
`Find help or get in touch with us`

#### Support section
##### Title 
`Support`

##### Bullet list (note: links are specific to language settings)
- `Looking for answers to your questions? [Visit our Help Center (English only)]`(https://adblockplus.org/redirect?link=help_center_abp_en)
- `Found a bug?`  `Send us a bug report` (opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=adblock_plus_report_bug` in a new tab)
- `Want support from our community?`  `Go to the Forum`  (depending on the application open the application specific [Documentation link](/spec/abp/prefs.md#documentation-link)  `%LINK%=firefox_support` `%LINK%=chrome_support` `%LINK%=opera_support` or `%LINK%=edge_support`in a new tab)
- `Email: support@adblockplus.org`

#### Get in touch section
##### Title 
`Get in touch`

##### Description 
`Have a question or a new idea? We're here to help.`

##### Social media icons and links
| Social Media | Link URL | Browser locale |
|----------------|----------------| ----------------|
| `Twitter` | Opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=social_twitter` in a new tab | For all locales |
| `Facebook` | Opens [Documentation link](/spec/abp/prefs.md#documentation-link) `%LINK%=social_facebook` in a new tab | For all locales |